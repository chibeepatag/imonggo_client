require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class ImonggoRestProvider

    include HttpClient
    def initialize(imonggo_domain, account_id, token, format='xml')
      @domain = get("#{imonggo_domain}/system/api_url?account_id=#{account_id}").body
      @token = token
      @clients = Hash.new
      @format = format
    end

    def get_products_client
      ProductClient.new(@domain, @token, @format)
    end

    def get_application_client
      ApplicationClient.new(@domain, @token, @format)
    end

    def get_branch_client
      BranchClient.new(@domain, @token, @format)
    end

    def get_customer_client
      CustomerClient.new(@domain, @token, @format)
    end

    def get_daily_sales_client
      DailySalesClient.new(@domain, @token, @format)
    end

    def get_inventory_client
      InventoryClient.new(@domain, @token, @format)
    end

    def get_invoice_client
      InvoiceClient.new(@domain, @token, @format)
    end

    def get_tax_setting_client
      TaxSettingClient.new(@domain, @token, @format)
    end
  end
end