require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class InventoryClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_inventories
      url ="#{@domain}/api/inventories.#{@format}"
      get(url, @token)
    end

    def get_inventories_by_parameters(parameters)
      url ="#{@domain}/api/inventories.#{@format}?"

      parameters.each do |key, value|
        if(!url.end_with?('?'))
          url.concat('&')
        end
        url.concat("#{key}=#{value}")
      end

      get(url, @token)
    end

  end
end