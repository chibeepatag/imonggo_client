require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class BranchClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_branches
      url = "#{@domain}/api/branches.#{@format}"
      get(url, @token)
    end

    def get_branch(branch_id)
      url = "#{@domain}/api/branches/#{branch_id}.#{@format}"
      get(url, @token)
    end
  end
end