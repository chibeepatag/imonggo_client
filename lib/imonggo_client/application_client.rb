require 'imonggo_client/http_client'

module ImonggoClient
  class ApplicationClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_application_settings doc_name
      url = "#{@domain}/api/application_settings/#{doc_name}.#{@format}"
      get(url, @token)
    end
  end
end