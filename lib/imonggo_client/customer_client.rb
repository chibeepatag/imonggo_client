require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class CustomerClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_customers
      url = "#{@domain}/api/customers.#{@format}"
      get(url, @token)
    end

    def get_customer(id)
      url = "#{@domain}/api/customers/#{id}.#{@format}"
      get(url, @token)
    end

    def get_customers_by_parameters(parameters)
      url ="#{@domain}/api/customers.#{@format}?"

      parameters.each do |key, value|
        if(!url.end_with?('?'))
          url.concat('&')
        end
        url.concat("#{key}=#{value}")
      end

      get(url, @token)
    end
  end
end