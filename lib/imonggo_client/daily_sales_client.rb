require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class DailySalesClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_daily_sales_summary(date, branch_id)
      url = "#{@domain}/api/daily_sales/#{date}.#{@format}?branch_id=#{branch_id}&extended=1"
      get(url, @token)
    end

  end
end