require 'net/http'

module ImonggoClient
  module HttpClient
    def get(url, token=nil)
      uri = URI(url)

      req = Net::HTTP::Get.new(uri)
      if(!token.nil?)
        req.basic_auth token, 'X'
      end
      Net::HTTP.start(uri.host, uri.port,
      :use_ssl => uri.scheme == 'https') do |http|
        response = http.request req # Net::HTTPResponse object
      end
    end
  end
end