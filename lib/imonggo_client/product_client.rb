require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class ProductClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_product(id)
      url = "#{@domain}/api/products/#{id}.#{@format}"
      get(url, @token)
    end

    def get_products
      url = "#{@domain}/api/products.#{@format}"
      get(url, @token)
    end

    def post_product

    end
  end
end