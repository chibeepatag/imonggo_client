require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class TaxSettingClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_taxes_setting
      url = "#{@domain}/api/tax_settings.#{@format}"
      get(url, @token)
    end

  end
end