require File.join(File.dirname(__FILE__), 'http_client.rb')

module ImonggoClient
  class InvoiceClient
    include HttpClient
    def initialize(domain, token, format)
      @domain = domain
      @token = token
      @format = format
    end

    def get_invoices
      url ="#{@domain}/api/invoices.#{@format}"
      get(url, @token)
    end

    def get_invoice(id)
      url ="#{@domain}/api/invoices/#{id}.#{@format}"
      get(url, @token)
    end

    def get_invoices_by_parameters(parameters)
      url ="#{@domain}/api/invoices.#{@format}?"

      parameters.each do |key, value|
        if(!url.end_with?('?'))
          url.concat('&')
        end
        url.concat("#{key}=#{value}")
      end

      get(url, @token)
    end

  end
end