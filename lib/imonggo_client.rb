require "imonggo_client/version"
require "imonggo_client/http_client"
require "imonggo_client/application_client"
require "imonggo_client/branch_client"
require "imonggo_client/customer_client"
require "imonggo_client/imonggo_rest_provider"
require "imonggo_client/inventory_client"
require "imonggo_client/invoice_client"
require "imonggo_client/product_client"
require "imonggo_client/tax_setting_client"
require "imonggo_client/daily_sales_client.rb"

require 'model/branch.rb'
require 'model/customer.rb'
require 'model/daily_sales_summary.rb'
require 'model/inventory.rb'
require 'model/invoice.rb'
require 'model/product.rb'
require 'model/tax_setting.rb'
require 'model/application_setting.rb'               

require 'util/string.rb'

module ImonggoClient
  # Your code goes here...
end
