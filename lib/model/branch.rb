require 'nokogiri'
require 'date'

module ImonggoModel
  class Branch
    attr_reader :city,
              :country,
              :id,
              :name,
              :site_type,
              :state,
              :status,
              :street,
              :subsribed_at,
              :subscription_type,
              :tin,
              :zipcode,
              :utc_created_at,
              :utc_updated_at
    def initialize(branch_response)
      doc = Nokogiri::XML(branch_response)
      @city = doc.xpath('//branch/city').inner_text.to_s
      @country = doc.xpath('//branch/country').inner_text.to_s
      @id = doc.xpath('//branch/id').inner_text.to_s
      @name = doc.xpath('//branch/name').inner_text.to_s
      @site_type = doc.xpath('//branch/site_type').inner_text.to_s
      @state = doc.xpath('//branch/state').inner_text.to_s
      @status = doc.xpath('//branch/status').inner_text.to_s
      @street = doc.xpath('//branch/street').inner_text.to_s
      @subscribed_at = doc.xpath('//branch/subscribed_at').inner_text.to_s
      @subscription_type = doc.xpath('//branch/subscription_type').inner_text.to_s
      @tin = doc.xpath('//branch/tin').inner_text.to_s
      @zipcode = doc.xpath('//branch/zipcode').inner_text.to_s

      created_at = doc.xpath('//branch/utc_created_at').inner_text.to_s
      @utc_created_at = Date.rfc3339(created_at)

      updated_at = doc.xpath('//branch/utc_updated_at').inner_text.to_s
      @utc_updated_at = Date.rfc3339(updated_at)
    end

  end
end