require 'date'
require 'nokogiri'

module ImonggoModel
  class Inventory
    attr_reader :inventory_items
    def initialize(inventories_response)
      doc = Nokogiri::XML(inventories_response)
      @inventory_items = Array.new

      inventory_nodes = doc.xpath('//inventories/inventory')
      inventory_nodes.each do |inventory_node|
        @inventory_items << Inventory_Item.new(inventory_node)
      end
    end
  end

  class Inventory_Item
    attr_reader :branch_id,
              :product_id,
              :quantity,
              :stock_no,
              :utc_created_at,
              :utc_updated_at
    def initialize(inventory_node)
      @branch_id = inventory_node.at_css('branch_id').content.to_s
      @product_id = inventory_node.at_css('product_id').content.to_s
      @quantity = inventory_node.at_css('quantity').content.to_f
      @stock_no = inventory_node.at_css('stock_no').content.to_s
      created_at = inventory_node.at_css('utc_created_at').content.to_s
      @utc_created_at = Date.rfc3339(created_at)
      updated_at = inventory_node.xpath('utc_updated_at').inner_text.to_s
      @utc_updated_at = Date.rfc3339(updated_at)
    end
  end
end