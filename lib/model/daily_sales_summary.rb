require 'bigdecimal'
require 'nokogiri'

module ImonggoModel
  class DailySalesSummary
    attr_reader :tax_inclusive,
              :average_amount_per_invoice,
              :payments,
              :tax,
              :quantity,
              :transaction_count,
              :amount,
              :taxes,
              :amount_with_tax,
              :amount_without_tax
    
    attr_accessor :date
                                
    def initialize(sales_summary_response)
      doc = Nokogiri::XML(sales_summary_response)
      @tax_inclusive = doc.xpath('//daily_sales/tax_inclusive').inner_text.to_s
      @average_amount_per_invoice = BigDecimal.new(doc.xpath('//daily_sales/average_amount_per_invoice').inner_text.to_s, 2)

      payment_nodes = doc.xpath('//daily_sales/payments/payment')
      @payments = Hash.new
      payment_nodes.each do |node|
        payment = SaleSummaryPayment.new(node)
        @payments[payment.payment_type] = payment
      end

      @tax = BigDecimal.new(doc.xpath('//daily_sales/tax').inner_text.to_s, 2)
      @quantity = BigDecimal.new(doc.xpath('//daily_sales/quantity').inner_text.to_s, 2)
      @transaction_count = doc.xpath('//daily_sales/transaction_count').inner_text.to_i
      @amount = BigDecimal.new(doc.xpath('//daily_sales/amount').inner_text.to_s, 2)

      tax_nodes = doc.xpath('//daily_sales/taxes/tax')
      @taxes = Array.new
      tax_nodes.each do |tax_node|
        @taxes << Tax.new(tax_node)
      end

      @amount_with_tax = BigDecimal.new(doc.xpath('daily_sales/amount_with_tax').inner_text.to_s, 2)
      @amount_without_tax = BigDecimal.new(doc.xpath('daily_sales/amount_without_tax').inner_text.to_s, 2)
    end
  end

  class SaleSummaryPayment
    attr_reader :quantity,
              :amount,
              :payment_type
    def initialize(payment_node)
      @quantity = payment_node.at_css('quantity').content.to_f unless payment_node.at_css('quantity').nil?
      @amount = BigDecimal.new(payment_node.at_css('amount').content.to_s, 2) unless payment_node.at_css('amount').nil?
      @payment_type = payment_node.at_css('payment_type').content.to_s unless payment_node.at_css('payment_type').nil?
    end
  end

  class Tax
    attr_reader :tax_rate_id,
              :tax_rate_name,
              :tax_rate_reference,
              :amount
    def initialize(tax_node)
      @tax_rate_id = tax_node.at_css('tax_rate_id').content.to_s  unless tax_node.at_css('tax_rate_id').nil?
      @tax_rate_name = tax_node.at_css('tax_rate_name').content.to_s unless tax_node.at_css('tax_rate_name').nil?
      @tax_rate_reference = tax_node.at_css('tax_rate_reference').content.to_s  unless tax_node.at_css('tax_rate_reference').nil? 
      @amount = BigDecimal.new(tax_node.at_css('amount').inner_text.to_s, 2)  unless tax_node.at_css('amount').nil?
    end
  end
end