require 'bigdecimal'
require 'nokogiri'
require 'date'
require File.join(File.dirname(__FILE__), '../util/string.rb')

module ImonggoModel
  class Invoice
    attr_reader :amount,
              :amount_with_tax,
              :discount_text,
              :id,
              :invoice_no,
              :layaway_status,
              :quantity,
              :reference,
              :remark,
              :salesman_id,
              :status,
              :tax,
              :tax_exempt,
              :tax_inclusive,
              :user_id,
              :voided_reason,
              :customer_name,
              :customer_id,
              :utc_invoice_date,
              :utc_post_date,
              :utc_created_at,
              :utc_updated_at,
              :payments,
              :invoice_lines,
              :invoice_tax_rates
    def initialize(invoice_response)
      doc = Nokogiri::XML(invoice_response)
      @amount = BigDecimal.new(doc.xpath('//invoice/amount').inner_text.to_s, 2)
      @amount_with_tax = BigDecimal.new(doc.xpath('//invoice/amount_with_tax').inner_text.to_s, 2)
      @discount_text = doc.xpath('//invoice/discount_text').inner_text.to_s
      @id = doc.xpath('//invoice/id').inner_text.to_s
      @invoice_no = doc.xpath('//invoice/invoice_no').inner_text.to_s
      @layaway_status = doc.xpath('//invoice/layaway_status').inner_text.to_s
      @quantity = doc.xpath('//invoice/quantity').inner_text.to_f
      @reference = doc.xpath('//invoice/reference').inner_text.to_s

      @remark = doc.xpath('//invoice/remark').inner_text.to_s
      @salesman_id = doc.xpath('//invoice/salesman_id').inner_text.to_s
      @status = doc.xpath('//invoice/status').inner_text.to_s

      @tax = BigDecimal.new(doc.xpath('//invoice/tax').inner_text.to_s, 2)
      @tax_exempt = doc.xpath('//invoice/tax_exempt').inner_text.to_s
      @tax_inclusive = doc.xpath('//invoice/tax_inclusive').inner_text.to_s.to_bool
      @user_id = doc.xpath('//invoice/user_id').inner_text.to_s
      @voided_reason = doc.xpath('//invoice/voided_reason').inner_text.to_s
      @customer_name = doc.xpath('//invoice/customer_name').inner_text.to_s
      @customer_id = doc.xpath('//invoice/customer_id').inner_text.to_s

      invoice_date = doc.xpath('//invoice/utc_invoice_date').inner_text.to_s
      @utc_invoice_date = Date.rfc3339(invoice_date)

      post_date = doc.xpath('//invoice/utc_post_date').inner_text.to_s
      @utc_post_date = Date.rfc3339(post_date)

      created_at = doc.xpath('//invoice/utc_created_at').inner_text.to_s
      @utc_created_at =  Date.rfc3339(created_at)

      updated_at = doc.xpath('//invoice/utc_updated_at').inner_text.to_s
      @utc_updated_at = Date.rfc3339(updated_at)

      @payments = Array.new
      payment_nodes = doc.xpath('//invoice/payments/payment')
      payment_nodes.each do |payment_node|
        payment = Payment.new(payment_node)
        @payments << payment
      end

      @invoice_lines = Array.new
      invoice_line_nodes = doc.xpath('//invoice/invoice_lines/invoice_line')
      invoice_line_nodes.each do |invoice_line_node|
        invoice_line = InvoiceLine.new(invoice_line_node)
        @invoice_lines << invoice_line
      end

      @invoice_tax_rates = Array.new
      invoice_tax_rate_nodes = doc.xpath('//invoice/invoice_tax_rates/invoice_tax_rate')
      invoice_tax_rate_nodes.each do |invoice_tax_rate_node|
        invoice_tax_rate = InvoiceTaxRate.new(invoice_tax_rate_node)
        @invoice_tax_rates << invoice_tax_rate
      end
    end
  end

  class Payment
    attr_reader :amount,
              :card_first_name,
              :card_last_4_digits,
              :card_last_name,
              :card_type,
              :city,
              :country,
              :payment_type,
              :reference,
              :state,
              :street,
              :tender,
              :payment_type_name
    def initialize(payment_node)
      @amount = BigDecimal.new(payment_node.at_css('amount').content.to_s, 2)
      @card_first_name = payment_node.at_css('card_first_name').content.to_s
      @card_last_4_digits = payment_node.at_css('card_last_4_digits').content.to_s
      @card_last_name = payment_node.at_css('card_last_name').content.to_s
      @card_type = payment_node.at_css('card_type').content.to_s
      @city = payment_node.at_css('city').content.to_s
      @country = payment_node.at_css('country').content.to_s
      @payment_type = payment_node.at_css('payment_type_id').content.to_s
      @reference = payment_node.at_css('reference').content.to_s
      @state = payment_node.at_css('state').content.to_s
      @street = payment_node.at_css('street').content.to_s
      @tender = BigDecimal.new(payment_node.at_css('tender').content.to_s, 2)
      @payment_type_name  = payment_node.at_css('payment_type_name').content.to_s
    end
  end

  class InvoiceLine
    attr_reader :discount_text,
              :line_no,
              :price,
              :product_id,
              :quantity,
              :remark,
              :retail_price,
              :subtotal,
              :product_name,
              :product_stock_no
    def initialize(invoice_line_node)
      @discount_text = invoice_line_node.at_css('discount_text').content.to_s
      @line_no = invoice_line_node.at_css('line_no').content.to_s
      @price   = BigDecimal.new(invoice_line_node.at_css('price').content.to_s, 2)
      @product_id = invoice_line_node.at_css('product_id').content.to_s
      @quantity = invoice_line_node.at_css('quantity').content.to_f
      @remark = invoice_line_node.at_css('remark').content.to_s
      @retail_price = BigDecimal.new(invoice_line_node.at_css('retail_price').content.to_s, 2)
      @subtotal = BigDecimal.new(invoice_line_node.at_css('subtotal').content.to_s, 2)
      @product_name = invoice_line_node.at_css('product_name').content.to_s
      @product_stock_no = invoice_line_node.at_css('product_stock_no').content.to_s
    end
  end

  class InvoiceTaxRate
    attr_reader :tax_rate_id,
              :rate,
              :amount,
              :name
    def initialize(invoice_tax_rate_node)
      @tax_rate_id = invoice_tax_rate_node.at_css('tax_rate_id').content.to_s
      @rate = BigDecimal.new(invoice_tax_rate_node.at_css('rate').content.to_s, 2)
      @amount = BigDecimal.new(invoice_tax_rate_node.at_css('amount').content.to_s, 2)
      @name = invoice_tax_rate_node.at_css('name').content.to_s
    end
  end
end