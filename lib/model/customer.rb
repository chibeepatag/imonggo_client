require 'nokogiri'
require 'date'
require File.join(File.dirname(__FILE__), '../util/string.rb')

module ImonggoModel
  class Customer
    attr_reader :alternate_code,
             :available_points,
             :birthday,
             :city,
             :code,
             :company_name,
             :country,
             :customer_type_id,
             :email,
             :fax,
             :first_name,
             :id,
             :last_name,
             :mobile,
             :name,
             :remark,
             :state,
             :status,
             :street,
             :tax_exempt,
             :telephone,
             :tin,
             :zipcode,
             :customer_type_name,
             :discount_text,
             :point_to_amount_ratio,
             :birthdate,
             :utc_created_at,
             :utc_updated_at
    def initialize(customer_response)
      doc = Nokogiri::XML(customer_response)
      @alternate_code  = doc.xpath('//customer/alternate_code').inner_text.to_s
      @available_points = doc.xpath('//customer/available_points').inner_text.to_f

      birthday_text = doc.xpath('//customer/birthday').inner_text.to_s
      @birthday = Date.rfc3339(birthday_text)

      @city = doc.xpath('//customer/city').inner_text.to_s
      @code = doc.xpath('//customer/code').inner_text.to_s
      @company_name = doc.xpath('//customer/company_name').inner_text.to_s
      @country = doc.xpath('//customer/country').inner_text.to_s
      @customer_type_id = doc.xpath('//customer/customer_type_id').inner_text.to_s
      @email = doc.xpath('//customer/email').inner_text.to_s
      @fax = doc.xpath('//customer/fax').inner_text.to_s
      @first_name = doc.xpath('//customer/first_name').inner_text.to_s
      @id = doc.xpath('//customer/id').inner_text.to_s
      @last_name = doc.xpath('//customer/last_name').inner_text.to_s
      @mobile = doc.xpath('//customer/mobile').inner_text.to_s
      @name = doc.xpath('//customer/name').inner_text.to_s
      @remark = doc.xpath('//customer/remark').inner_text.to_s
      @state = doc.xpath('//customer/state').inner_text.to_s
      @status = doc.xpath('//customer/status').inner_text.to_s
      @street = doc.xpath('//customer/street').inner_text.to_s
      @tax_exempt = doc.xpath('//customer/tax_exempt').inner_text.to_s.to_bool
      @telephone = doc.xpath('//customer/telephone').inner_text.to_s
      @tin = doc.xpath('//customer/tin').inner_text.to_s
      @zipcode = doc.xpath('//customer/zipcode').inner_text.to_s
      @customer_type_name = doc.xpath('//customer/customer_type_name').inner_text.to_s
      @discount_text = doc.xpath('//customer/discount_text').inner_text.to_s
      @point_to_amount_ratio = doc.xpath('//customer/point_to_amount_ratio').inner_text.to_s
      @birthdate = doc.xpath('//customer/birthdate').inner_text.to_s

      created_at = doc.xpath('//customer/utc_created_at').inner_text.to_s
      @utc_created_at = Date.rfc3339(created_at)

      updated_at = doc.xpath('//customer/utc_updated_at').inner_text.to_s
      @utc_updated_at = Date.rfc3339(updated_at)
    end
  end
end