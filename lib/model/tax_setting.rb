require 'nokogiri'
require 'bigdecimal'
require File.join(File.dirname(__FILE__), '../util/string.rb')

module ImonggoModel
  class TaxSetting
    attr_reader :compute_tax,
              :tax_inclusive,
              :tax_rates
    def initialize(tax_setting_response)
      doc = Nokogiri::XML(tax_setting_response)
      @compute_tax = doc.xpath('//compute_tax').inner_text.to_s.to_bool
      @tax_inclusive = doc.xpath('//tax_inclusive').inner_text.to_s.to_bool
      @tax_rates = Array.new
      tax_rate_nodes = doc.xpath('//tax_rates/tax_rate')
      tax_rate_nodes.each do |tax_rate_node|
        @tax_rates << TaxRate.new(tax_rate_node)
      end
    end
  end

  class TaxRate
    TAX_RATE_TYPE = {
      '0' => 'All products',
      '1' => 'Selected products'
    }

    attr_reader :branch_id,
              :id,
              :name,
              :reference,
              :status,
              :value,
              :tax_rate_type

    def initialize(tax_rate_node)
      @branch_id = tax_rate_node.at_css('branch_id').content.to_s
      @id = tax_rate_node.at_css('id').content.to_s
      @name = tax_rate_node.at_css('name').content.to_s
      @reference = tax_rate_node.at_css('reference').content.to_s
      @status = tax_rate_node.at_css('status').content.to_s
      @value =  BigDecimal.new(tax_rate_node.at_css('value').content.to_s, 2)
      tax_rate_type = tax_rate_node.at_css('tax_rate_type').content.to_s
      @tax_rate_type =  TAX_RATE_TYPE[tax_rate_type]

    end
  end
end