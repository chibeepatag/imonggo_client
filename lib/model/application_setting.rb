require 'nokogiri'

module ImonggoModel
  class ApplicationSetting
    attr_reader :settings
    
    def initialize(application_setting_response)
      @settings = Hash.new
      
      doc = Nokogiri::XML(application_setting_response)
      child_nodes = doc.xpath('//settings').children
      child_nodes.each do |child_node|
        @settings[child_node.name] = child_node.inner_text.to_s
      end
    end
  end
end