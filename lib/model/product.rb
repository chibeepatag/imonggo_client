require 'bigdecimal'
require 'nokogiri'
require 'date'
require File.join(File.dirname(__FILE__), '../util/string.rb')
require File.join(File.dirname(__FILE__), 'tax_setting.rb')

module ImonggoModel
  class Product
    attr_reader :allow_decimal_quantities,
              :cost,
              :description,
              :disable_discount,
              :disable_inventory,
              :enable_open_price,
              :id,
              :name,
              :retail_price,
              :status,
              :stock_no,
              :tax_exempt,
              :tag_list,
              :barcode_list,
              :utc_created_at,
              :utc_updated_at,
              :thumbnail_url,
              :tax_rates,
              :branch_prices
    def initialize(product_response)
      doc = Nokogiri::XML(product_response)
      @allow_decimal_quantities = doc.xpath('//product/allow_decimal_quantities').inner_text.to_s.to_bool
      @cost = BigDecimal.new(doc.xpath('//product/cost').inner_text.to_s, 2)
      @description = doc.xpath('//product/description').inner_text.to_s
      @disable_discount = doc.xpath('//product/disable_discount').inner_text.to_s.to_bool
      @disable_inventory = doc.xpath('//product/disable_inventory').inner_text.to_s.to_bool
      @enable_open_price = doc.xpath('//product/enable_open_price').inner_text.to_s.to_bool
      @id = doc.xpath('//product/id').inner_text.to_s
      @name = doc.xpath('//product/name').inner_text.to_s
      @retail_price = BigDecimal.new(doc.xpath('//product/retail_price').inner_text.to_s, 2)
      @status = doc.xpath('//product/status').inner_text.to_s
      @stock_no = doc.xpath('//product/stock_no').inner_text.to_s
      @tax_exempt = doc.xpath('//product/tax_exempt').inner_text.to_s.to_bool

      @tag_list = doc.xpath('//product/tag_list').inner_text.to_s.split(', ')
      @barcode_list = doc.xpath('//product/barcode_list').inner_text.to_s.split(', ')

      created_at = doc.xpath('//product/utc_created_at').inner_text.to_s
      @utc_created_at = Date.rfc3339(created_at)

      updated_at = doc.xpath('//product/utc_updated_at').inner_text.to_s
      @utc_updated_at = Date.rfc3339(updated_at)

      @thumbnail_url = doc.xpath('//product/thumbnail_url').inner_text.to_s

      @tax_rates = Array.new
      tax_nodes = doc.xpath('//product/tax_rates')
      tax_nodes.each do |tax_node|
        @tax_rates << TaxRate.new(tax_node)
      end
    end

  end
end