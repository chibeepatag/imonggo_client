require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/model/branch.rb')

class BranchTest < MiniTest::Unit::TestCase
  
  def setup
    filename = "/response/branch_response.xml"
    branch_response = File.read(File.join(File.dirname(__dir__), filename))
    @branch = ImonggoModel::Branch.new(branch_response)
  end
  
  def test_parse_branch
    refute_nil(@branch)
    assert_equal('Brighton', @branch.city)
    assert_equal('CA', @branch.country)
    assert_equal('23115', @branch.id)
    assert_equal('Ash Creek Arcade', @branch.name)
    assert_equal('AL', @branch.state)
    assert_equal('A', @branch.status)
    assert_equal('14 Hurston Drive', @branch.street)
    assert_equal('2', @branch.subscription_type)
    assert_equal('10923', @branch.zipcode)
    
    expected_created_at = Date.rfc3339('2015-07-01T05:34:37Z')
    assert_equal(expected_created_at, @branch.utc_created_at)
    
    expected_updated_at = Date.rfc3339('2015-07-09T03:42:53Z')
    assert_equal(expected_updated_at, @branch.utc_updated_at)    
  end
end