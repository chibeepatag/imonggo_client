require 'minitest/autorun'
require 'date'

require File.join(File.dirname(__FILE__), '../../lib/model/daily_sales_summary.rb')

class DailySalesSummaryTest < MiniTest::Unit::TestCase
  def test_parse_daily_sales_summary
    
    daily_sales_response = File.read(File.join(File.dirname(__dir__),'/response/daily_sales_summary_response.xml'))
    daily_sales_summary = ImonggoModel::DailySalesSummary.new(daily_sales_response)
    assert_equal('false', daily_sales_summary.tax_inclusive)

    expected_amount_per_invoice = BigDecimal.new('3236.96', 2)
    assert_equal(expected_amount_per_invoice, daily_sales_summary.average_amount_per_invoice)

    assert_equal(2, daily_sales_summary.payments.size)
    assert_equal(BigDecimal.new('7302.94', 2), daily_sales_summary.payments['Cash'].amount)
    assert_equal(2, daily_sales_summary.payments['Cash'].quantity)
    assert_equal('Cash', daily_sales_summary.payments['Cash'].payment_type)    
    assert_equal(BigDecimal.new('2407.94', 2), daily_sales_summary.payments['Credit Card'].amount)
    assert_equal(1, daily_sales_summary.payments['Credit Card'].quantity)
    assert_equal('Credit Card', daily_sales_summary.payments['Credit Card'].payment_type)
    
    
    expected_tax = BigDecimal.new('682.08', 2)
    assert_equal(expected_tax, daily_sales_summary.tax)
    
    expected_quantity = BigDecimal.new('6.0', 1)
    assert_equal(expected_quantity, daily_sales_summary.quantity)
    
    expected_transaction_count = 3
    assert_equal(expected_transaction_count, daily_sales_summary.transaction_count)
    
    expected_amount = BigDecimal.new('9028.8', 2)
    assert_equal(expected_amount, daily_sales_summary.amount)
    
    assert_equal(3, daily_sales_summary.taxes.size)
    assert_equal('843', daily_sales_summary.taxes[0].tax_rate_id)
    assert_equal('Vat', daily_sales_summary.taxes[0].tax_rate_name)
    assert_equal('VAT', daily_sales_summary.taxes[0].tax_rate_reference)
    expected_vat_amount = BigDecimal.new('496.06', 2)
    assert_equal(expected_vat_amount, daily_sales_summary.taxes[0].amount)            
    
    expected_amount_with_tax = BigDecimal.new('9710.88', 2)
    assert_equal(expected_amount_with_tax, daily_sales_summary.amount_with_tax)
    expected_amount_without_tax = BigDecimal.new('9028.8', 2)
    assert_equal(expected_amount_without_tax, daily_sales_summary.amount_without_tax)
  end
  
  def test_parse_empty_daily_sales
    daily_sales_response = File.read(File.join(File.dirname(__dir__),'/response/daily_sales_summary1.xml'))
    daily_sales_summary = ImonggoModel::DailySalesSummary.new(daily_sales_response)
    refute_nil daily_sales_summary
    daily_sales_summary.date = Date.today
    assert_equal(Date.today, daily_sales_summary.date)
  end

end