require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/model/application_setting.rb')

class ApplicationSettingTest < MiniTest::Unit::TestCase
  
  def setup
    filename = "/response/application_setting_response.xml"
    application_setting_response = File.read(File.join(File.dirname(__dir__), filename))
    @application_setting = ImonggoModel::ApplicationSetting.new(application_setting_response)
  end
  
  def test_parse_application_setting
    refute_nil(@application_setting)
    settings = @application_setting.settings
    assert_equal('SLS', settings['sales'])   
    assert_equal('DBC', settings['payment_type_debit'])  
    assert_equal('CHK', settings['payment_type_check'])
    assert_equal('PNT', settings['payment_type_points'])     
  end
end