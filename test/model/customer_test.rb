require 'minitest/autorun'

require File.join(File.dirname(__FILE__), '../../lib/model/customer.rb')

class DailySalesSummaryTest < MiniTest::Unit::TestCase
  
  def setup
    
    customer_response = File.read(File.join(File.dirname(__dir__), '/response/customer_response.xml'))
    @customer = ImonggoModel::Customer.new(customer_response)
  end
  
  def test_parse_customer
    refute_nil(@customer)
    assert_equal('192', @customer.alternate_code)
    assert_equal(387.2, @customer.available_points)
    assert_equal('Unley', @customer.city)
    assert_equal('AU', @customer.country)
    assert_equal('1128', @customer.customer_type_id)
    assert_equal('philip.gleason@gmail.com', @customer.email)
    assert_equal('Philip', @customer.first_name)
    assert_equal('Gleason', @customer.last_name)
    assert_equal('362536', @customer.id)
    assert_equal('12 Leader Rd.', @customer.street)
    assert_equal(true, @customer.tax_exempt)
    assert_equal('9246658', @customer.telephone)
    assert_equal('5031', @customer.zipcode)
    assert_equal('MIKEE GROUP', @customer.customer_type_name)
    assert_equal('1.0%', @customer.discount_text)
    assert_equal('1.0', @customer.point_to_amount_ratio)
    assert_equal('1980-07-30', @customer.birthdate)
    
    expected_created_at =  Date.rfc3339('2015-07-30T04:21:35Z')
    assert_equal(expected_created_at, @customer.utc_created_at)
    
    expected_updated_at =  Date.rfc3339('2015-07-30T04:21:35Z')
    assert_equal(expected_updated_at, @customer.utc_updated_at)
  end
  
end