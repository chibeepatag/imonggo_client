require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/model/inventory.rb')

class InventoriesTest < MiniTest::Unit::TestCase
  def setup
    inventory_response = File.read(File.join(File.dirname(__dir__), '/response/inventory_response.xml'))
    @inventory = ImonggoModel::Inventory.new(inventory_response)
  end
  
  def test_parse_inventory
    refute_nil(@inventory)
    
    inventory_item = @inventory.inventory_items[0]                                
    assert_equal('23115', inventory_item.branch_id)
    assert_equal('949715', inventory_item.product_id)
    assert_equal(-4.0, inventory_item.quantity)
    assert_equal('4', inventory_item.stock_no)
    
    assert_equal(2015, inventory_item.utc_created_at.year)
  end
end