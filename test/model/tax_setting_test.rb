require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/model/tax_setting.rb')

class TaxSettingTest < MiniTest::Unit::TestCase
  
  def setup
    tax_setting_response = File.read(File.join(File.dirname(__dir__), '/response/tax_setting_response.xml'))
    @tax_setting = ImonggoModel::TaxSetting.new(tax_setting_response)     
  end
  
  def test_parse_tax_setting
    refute_nil(@tax_setting)        
    assert(@tax_setting.compute_tax)
    assert(!@tax_setting.tax_inclusive)
  end
  
  def test_parse_tax_rate
    assert_equal(3, @tax_setting.tax_rates.size)
    tax_rate = @tax_setting.tax_rates[0]
    assert_equal('843', tax_rate.id)
    assert_equal('VAT', tax_rate.reference)
    assert_equal('Vat', tax_rate.name)
    
    expected_value = BigDecimal.new('0.12', 2)
    assert_equal(expected_value, tax_rate.value)
  end
end