require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/model/invoice.rb')

class InvoiceTest < MiniTest::Unit::TestCase
  
  def setup
    invoice_response = File.read(File.join(File.dirname(__dir__), '/response/invoice_response.xml'))
    @invoice = ImonggoModel::Invoice.new(invoice_response)
  end
  
  def test_parse_invoice
    refute_nil(@invoice)
    
    expected_amount = BigDecimal.new('6201.0', 2)
    assert_equal(expected_amount, @invoice.amount)
    
    expected_amount_with_tax = BigDecimal.new('6591.4', 2)
    assert_equal(expected_amount_with_tax, @invoice.amount_with_tax)
    
    assert_equal('2676542', @invoice.id)
    assert_equal('85', @invoice.invoice_no)
    assert_equal(3.0, @invoice.quantity)
    assert_equal('23936', @invoice.salesman_id)
    assert_equal('S', @invoice.status)
    
    expected_tax = BigDecimal.new('390.4', 2)
    assert_equal(expected_tax, @invoice.tax)
    assert(!@invoice.tax_inclusive)
    
    assert_equal('23936', @invoice.user_id)
    
    assert_equal(7, @invoice.utc_invoice_date.month)
    assert_equal(2015, @invoice.utc_invoice_date.year)
  end
  
  def test_parse_invoice_payments
    assert_equal(3, @invoice.payments.size)
    
    payment = @invoice.payments[0]
    expected_amount = BigDecimal.new('2541.0', 2)
    assert_equal(expected_amount, payment.amount)
  end
  
  def test_parse_invoice_line
    assert_equal(3, @invoice.invoice_lines.size)
    invoice_line = @invoice.invoice_lines[0]
    expected_price = BigDecimal.new('2780.0', 2)
    assert_equal(expected_price, invoice_line.price)
    assert_equal('949802', invoice_line.product_id)    
  end
  
  def test_parse_invoice_tax_rate
    assert_equal(3, @invoice.invoice_tax_rates.size)
    tax_rate = @invoice.invoice_tax_rates[0]
    assert_equal('843', tax_rate.tax_rate_id)
    assert_equal('Vat', tax_rate.name)
  end
end