require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/model/product.rb')

class ProductTest < MiniTest::Unit::TestCase
  def setup
    product_response = File.read(File.join(File.dirname(__dir__), '/response/product_response.xml'))
    @product = ImonggoModel::Product.new(product_response)
  end

  def test_create_product    
    refute_nil(@product)

    assert(@product.allow_decimal_quantities)

    expected_cost = BigDecimal.new('2420.0', 2)
    assert_equal(expected_cost, @product.cost)

    expected_description = 'Oster 2984 mixer choper and whisk'
    assert_equal(expected_description, @product.description)

    assert(@product.disable_discount)
    assert(!@product.disable_inventory)
    assert(@product.enable_open_price)

    assert_equal('949802', @product.id)
    expected_name = 'Oster Stick Mixer with Chopper and Whisk'
    assert_equal(expected_name, @product.name)

    expected_retail_price = BigDecimal.new('2780.0', 2)
    assert_equal(expected_retail_price, @product.retail_price)

    assert_equal('E001', @product.stock_no)
    assert(@product.tax_exempt)

    assert_equal(2, @product.tag_list.size)
    assert_equal('mixer', @product.tag_list[0])
    assert_equal('chopper', @product.tag_list[1])
    assert_equal('4890', @product.barcode_list[0])

    expected_created_at = Date.rfc3339('2015-07-09T01:59:59Z')
    assert_equal(expected_created_at, @product.utc_created_at)

    expected_updated_at = Date.rfc3339('2015-07-29T09:17:36Z')
    assert_equal(expected_updated_at, @product.utc_updated_at)
  end

  def test_create_product_tax
    taxes = @product.tax_rates
    assert_equal(1, taxes.size)
    tax = @product.tax_rates[0]
    assert_equal('845', tax.id)
    assert_equal('State', tax.name)
    
    expected_value = BigDecimal.new(0.015, 3)
    assert_equal(expected_value, tax.value)
  end
end