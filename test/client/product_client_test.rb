require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/product_client.rb')

class ProductClientTest < MiniTest::Unit::TestCase
  def setup
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @product_client = ImonggoClient::ProductClient.new(domain, token, format)
  end
  
  def test_get_products    
    response = @product_client.get_products()
    assert_equal('200', response.code)
  end
  
  def test_get_product
    id = 949712    
    response = @product_client.get_product(id)
    assert_equal('200', response.code)
  end
end
