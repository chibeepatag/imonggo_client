require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/customer_client.rb')

class CustomerClientTest < MiniTest::Unit::TestCase
  def setup    
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @customer_client = ImonggoClient::CustomerClient.new(domain, token, format)
  end
  
  def test_get_customers    
    response = @customer_client.get_customers()
    assert_equal('200', response.code)
  end
  
  def test_get_customer
    id = 362536    
    response = @customer_client.get_customer(id)    
    assert_equal('200', response.code)
  end
  
  def test_get_customers_by_parameters
    parameters = {
      'from' => '2014-07-01T00:00:00Z',
      'to' => '2016-07-30T00:00:00Z',
      'active_only' => 1
    }
    response = @customer_client.get_customers_by_parameters(parameters)
    
    assert_equal('200', response.code)    
  end
end
