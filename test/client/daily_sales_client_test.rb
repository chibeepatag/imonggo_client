require 'minitest/autorun'
require 'nokogiri'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/daily_sales_client.rb')
class DailySalesClientTest < MiniTest::Unit::TestCase
    
  def setup    
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @daily_sales_client = ImonggoClient::DailySalesClient.new(domain, token, format)
  end
  
  def test_get_daily_sales_summary
    date = '2015-09-17'
    branch_id = 23115
    response = @daily_sales_client.get_daily_sales_summary(date, branch_id)
    assert_equal('200', response.code)
    doc = Nokogiri::XML(response.body)
    payments = doc.xpath('//daily_sales').at_css('payments')
    refute_nil(payments)
  end
end