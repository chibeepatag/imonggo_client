require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/inventory_client.rb')

class InventoryClientTest < MiniTest::Unit::TestCase
  def setup    
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @inventory_client = ImonggoClient::InventoryClient.new(domain, token, format)
  end

  def test_get_inventories
    response = @inventory_client.get_inventories()
    assert_equal('200', response.code)    
  end
  
  def test_get_inventories_by_parameters
    parameters = {
      'page' => 1,
      'before' => '2015-07-30T06:32:00Z',
      'after' => '2011-01-01T00:00:00Z',      
    }
    response = @inventory_client.get_inventories_by_parameters(parameters)    
    assert_equal('200', response.code)        
  end


end