require 'minitest/autorun'
require 'minitest/unit'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/http_client.rb')

class TestClass
  include ImonggoClient::HttpClient
end

class HttpClientTest < MiniTest::Unit::TestCase
  def setup
    @instance = TestClass.new
  end
  
  def test_get
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    url = 'http://rpatag.imonggo.net:8888/api/products.xml'
    response = @instance.get(url, token)
    assert_equal('200', response.code)
    
    domain = @instance.get('http://www.imonggo.net:8888/system/api_url?account_id=rpatag  ')
    assert_equal('http://rpatag.imonggo.net:8888', domain.body)
  end
end