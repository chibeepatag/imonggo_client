require 'minitest/autorun'
require 'nokogiri'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/branch_client.rb')

class BranchClientTest < MiniTest::Unit::TestCase
  def setup    
    @domain = 'http://rpatag.imonggo.net:8888'
    @token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    @format = 'xml'
    @branch_client = ImonggoClient::BranchClient.new(@domain, @token, @token)
  end
  
  def test_get_branches
    response = @branch_client.get_branches
    assert_equal('200', response.code)
    
    doc = Nokogiri::XML(response.body)
    branch = doc.xpath('//branches').at_css('branch')
    refute_nil(branch)
  end
  
  def test_get_branch
    id = 23115    
    response = @branch_client.get_branch(id)
    assert_equal('200', response.code)
    
    doc = Nokogiri::XML(response.body)
    city = doc.xpath('//branch').at_css('city')
    refute_nil(city)
  end
  
end
