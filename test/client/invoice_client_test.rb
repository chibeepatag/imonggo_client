require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/invoice_client.rb')

class InvoiceClientTest < MiniTest::Unit::TestCase
  def setup    
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @invoice_client = ImonggoClient::InvoiceClient.new(domain, token, format)
  end

  def test_get_invoices    
    response = @invoice_client.get_invoices()
    assert_equal('200', response.code)
  end

  def test_get_invoice
    id = 2676250
    response = @invoice_client.get_invoice(id)
    assert_equal('200', response.code)
  end

  def test_get_invoices_by_parameters
    parameters = {
      'from' => '2015-07-01T00:00:00Z',
      'to' => '2015-07-30T00:00:00Z',
      'q' => 'count'
    }
    response = @invoice_client.get_invoices_by_parameters(parameters)
    assert_equal('200', response.code)    
  end
  
end