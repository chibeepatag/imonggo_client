require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/application_client.rb')

class ApplicationClientTest < MiniTest::Unit::TestCase
  def setup
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @application_client = ImonggoClient::ApplicationClient.new(domain, token, format)
  end
  
  def test_get_application_settings       
    response = @application_client.get_application_settings()    
    assert_equal('200', response.code)     
  end
  
end
