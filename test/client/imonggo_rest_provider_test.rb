require 'minitest/autorun'
require 'nokogiri'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/imonggo_rest_provider.rb')

class ImonggoRestProviderTest < MiniTest::Unit::TestCase
  def setup
    imonggo_domain = 'http://www.imonggo.net:8888/system/api_url?account_id=rpatag'
    account_id = 'rpatag'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    @rest_provider = ImonggoClient::ImonggoRestProvider.new(imonggo_domain, account_id, token)
  end
  
  def test_rest_provider    
    refute_nil(@rest_provider)
  end
  
end