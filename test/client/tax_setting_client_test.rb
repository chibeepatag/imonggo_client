require 'minitest/autorun'
require File.join(File.dirname(__FILE__), '../../lib/imonggo_client/tax_setting_client.rb')
require 'nokogiri'

class TaxSettingClientTest < MiniTest::Unit::TestCase
  def setup    
    domain = 'http://rpatag.imonggo.net:8888'
    token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
    format = 'xml'
    @tax_setting_client = ImonggoClient::TaxSettingClient.new(domain, token, format)
  end
  
  def test_get_taxes_setting
    response = @tax_setting_client.get_taxes_setting()
    assert_equal('200', response.code)
    doc = Nokogiri::XML(response.body)
    payments = doc.xpath('//tax_rates').at_css('tax_rate')
    refute_nil(payments);   
  end
  
end
