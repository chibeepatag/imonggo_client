# Imonggo REST Client

## Implemented GET requests 
### (get all, get by id, get by parameters) and parsing of response for
	1. API URL 
	2. Branch
	3. Customer
	4. Daily Sales Summary 
	5. Inventory
	6. Invoice
	7. Product
	8. Tax Setting	

## How to use:
---
Instantiate ImonggoRestProviderTest
	imonggo_domain = 'http://www.imonggo.net:8888'
	account_id = 'rpatag'
	token = '96443594d8504d891fa0e0d4931bdc83367fcc51'
	rest_provider = ImonggoClient::ImonggoRestProvider.new(imonggo_domain, account_id, token)
### To get your Application Settings
    application_client = rest_provider.get_application_client.get_application_settings

### To get a specific resource
    product_client = rest_provider.get_products_client
    response = product_client.get_product(domain, token, id)
    product = Product.new(response)
	
	domain - Your imonggo API URL
	token - The API token associated to your account
	id - Your imonggo id
	
## To use as a gem
gem build imonggo_client.gemspec   
gem install ./imonggo_client-0.0.1.gem   
..\project_folder> bundle install